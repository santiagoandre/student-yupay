import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { logNavego } from './services/log'
import { SessionProvider } from './contexts/session'

const AppWrap = props => {
  useEffect(() => {
    logNavego(props.history.location.pathname + props.history.location.search)
    const unlisten = props.history.listen(location => {
      logNavego(location.pathname + location.search)
    })
    return () => {
      unlisten()
    }
  }, [])
  return <SessionProvider {...props} />
}
export default withRouter(AppWrap)
