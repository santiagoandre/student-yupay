import React from 'react'
import styled from 'styled-components'
import palette from '../../utils/palette'

export default () => {
  return (
    <Wrapper>
      <RightContent>
        <HappyFace />
        <Text>¡Correcto!</Text>
      </RightContent>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.1);
  z-index: 999;
`

const RightContent = styled.div`
  width: 410px;
  height: 300px;
  background: ${palette.primaryPurple};
  flex-direction: column;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: ${palette.white};
  margin: 100px;
  box-shadow: ${palette.secundaryBoxShadow};
`
const Text = styled.div`
  font-weight: bold;
  font-size: 40px;
  margin: 5px 15px;
`
const HappyFace = styled.img.attrs({
  src: require('../../assetsStudent/happy.svg'),
  alt: 'Cara triste',
})`
  width: 140px;
  height: 140px;
  margin: 5px;
`
