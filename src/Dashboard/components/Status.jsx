import React, { useState } from 'react'
import styled from 'styled-components'
import palette from '../../utils/palette'

export default ({ rightAnswers, coins }) => {
  return (
    <StatusContainer>
      <Text>Buen trabajo! Ganaste {coins}</Text> <CoinImg />
      <br />
      <Text>y {rightAnswers} preguntas correctas</Text>
    </StatusContainer>
  )
}

const StatusContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 200px;
`
const Text = styled.h1`
  color: ${palette.secundaryBlack};
`
const CoinImg = styled.img.attrs({
  src: require('../../assetsStudent/ic_moneda.svg'),
})`
  width: 60px;
  height: 60px;
  margin: 18px 0px;
`
