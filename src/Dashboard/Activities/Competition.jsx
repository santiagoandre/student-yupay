import React from 'react'
import styled from 'styled-components'
import NavBar from '../NavBar/index'
import Activity from './Activity'
import palette from '../../utils/palette'

export default () => {
  return (
    <ActivityContent>
      <NavBar />
      <Activity activity="competition" idSubtopic="identificador del tema" />
    </ActivityContent>
  )
}
const ActivityContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${palette.white};
  width: 100%;
  height: 100%;
`
