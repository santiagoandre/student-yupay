import React from 'react'
import {
  sessionInfo,
  createSession,
  removeSession as _removeSession,
} from '../services/session'

const SessionCtx = React.createContext()

export function useSession() {
  const context = React.useContext(SessionCtx)
  if (!context) {
    throw new Error(`useSession must be used within a SessionProvider`)
  }
  return context
}

export function SessionProvider(props) {
  const [session, _setSession] = React.useState(sessionInfo)

  const setSession = info => {
    _setSession(info)
    createSession(info)
  }

  const removeSession = () => {
    _setSession(undefined)
    _removeSession()
  }

  const addCoins = coins => {
    setSession({ ...session, coins: session.coins + coins })
  }

  return (
    <SessionCtx.Provider
      value={{ session, setSession, removeSession, addCoins }}
      {...props}
    />
  )
}
